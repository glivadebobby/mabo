package in.glivade.mabo;

import static in.glivade.mabo.app.Activity.launchClearStack;
import static in.glivade.mabo.app.Api.KEY_AGE;
import static in.glivade.mabo.app.Api.KEY_BP;
import static in.glivade.mabo.app.Api.KEY_DEVICE_ID;
import static in.glivade.mabo.app.Api.KEY_EMERGENCY_CONTACT1;
import static in.glivade.mabo.app.Api.KEY_EMERGENCY_CONTACT2;
import static in.glivade.mabo.app.Api.KEY_EMERGENCY_CONTACT3;
import static in.glivade.mabo.app.Api.KEY_HEIGHT;
import static in.glivade.mabo.app.Api.KEY_ID;
import static in.glivade.mabo.app.Api.KEY_NAME;
import static in.glivade.mabo.app.Api.KEY_PASSWORD;
import static in.glivade.mabo.app.Api.KEY_PHONE;
import static in.glivade.mabo.app.Api.KEY_POCKET_TYPE;
import static in.glivade.mabo.app.Api.KEY_TOKEN;
import static in.glivade.mabo.app.Api.SIGN_UP;
import static in.glivade.mabo.app.Constant.VALUE_PANT;
import static in.glivade.mabo.app.Constant.VALUE_SHIRT;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import in.glivade.mabo.app.AppController;
import in.glivade.mabo.app.PreferenceManager;
import in.glivade.mabo.app.VolleyErrorHandler;
import in.glivade.mabo.helper.MacAddress;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignUpActivity extends AppCompatActivity
        implements View.OnClickListener {

    private Context mContext;
    private EditText mEditTextName, mEditTextPhone, mEditTextPassword, mEditTextEmergencyContact1,
            mEditTextEmergencyContact2, mEditTextEmergencyContact3, mEditTextAge, mEditTextHeight;
    private RadioGroup mGroupBp, mGroupPocketType;
    private Button mButtonSignUp;
    private AnimationDrawable mAnimationDrawable;
    private PreferenceManager mPreferenceManager;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initObjects();
        initCallbacks();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAnimationDrawable != null && !mAnimationDrawable.isRunning()) {
            mAnimationDrawable.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAnimationDrawable != null && mAnimationDrawable.isRunning()) {
            mAnimationDrawable.stop();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == mButtonSignUp) {
            processSignUp();
        }
    }

    private void initObjects() {
        mEditTextName = (EditText) findViewById(R.id.input_name);
        mEditTextPhone = (TextInputEditText) findViewById(R.id.input_phone);
        mEditTextPassword = (TextInputEditText) findViewById(R.id.input_password);
        mEditTextEmergencyContact1 = (TextInputEditText) findViewById(
                R.id.input_emergency_contact_1);
        mEditTextEmergencyContact2 = (TextInputEditText) findViewById(
                R.id.input_emergency_contact_2);
        mEditTextEmergencyContact3 = (TextInputEditText) findViewById(
                R.id.input_emergency_contact_3);
        mEditTextAge = (TextInputEditText) findViewById(R.id.input_age);
        mEditTextHeight = (TextInputEditText) findViewById(R.id.input_height);
        mGroupBp = (RadioGroup) findViewById(R.id.grp_bp);
        mGroupPocketType = (RadioGroup) findViewById(R.id.grp_pocket_type);
        mButtonSignUp = (Button) findViewById(R.id.btn_sign_up);

        mContext = this;
        mAnimationDrawable = (AnimationDrawable) findViewById(
                R.id.activity_sign_up).getBackground();
        mPreferenceManager = new PreferenceManager(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mButtonSignUp.setOnClickListener(this);
    }

    private void processSignUp() {
        String name = mEditTextName.getText().toString().trim();
        String phone = mEditTextPhone.getText().toString().trim();
        String pass = mEditTextPassword.getText().toString().trim();
        String emergencyContact1 = mEditTextEmergencyContact1.getText().toString().trim();
        String emergencyContact2 = mEditTextEmergencyContact2.getText().toString().trim();
        String emergencyContact3 = mEditTextEmergencyContact3.getText().toString().trim();
        String age = mEditTextAge.getText().toString().trim();
        String height = mEditTextHeight.getText().toString().trim();
        boolean bp = false;
        if (mGroupBp.getCheckedRadioButtonId() == R.id.radio_yes) bp = true;
        String pocketType = VALUE_SHIRT;
        if (mGroupPocketType.getCheckedRadioButtonId() == R.id.radio_pant) pocketType = VALUE_PANT;

        if (validateInput(name, phone, pass, emergencyContact1, emergencyContact2,
                emergencyContact3, age, height)) {
            showProgressDialog("Signing up..");
            signUpUser(getSignUpRequestJson(name, phone, pass, emergencyContact1, emergencyContact2,
                    emergencyContact3, age, height, bp, pocketType));
        }
    }

    private boolean validateInput(String name, String phone, String pass, String emergencyContact1,
            String emergencyContact2, String emergencyContact3, String age, String height) {
        if (TextUtils.isEmpty(name)) {
            mEditTextName.requestFocus();
            mEditTextName.setError(getString(R.string.error_empty));
            return false;
        } else if (TextUtils.isEmpty(phone)) {
            mEditTextPhone.requestFocus();
            mEditTextPhone.setError(getString(R.string.error_empty));
            return false;
        } else if (phone.length() < 10) {
            mEditTextPhone.requestFocus();
            mEditTextPhone.setError(getString(R.string.error_phone_length));
            return false;
        } else if (TextUtils.isEmpty(pass)) {
            mEditTextPassword.requestFocus();
            mEditTextPassword.setError(getString(R.string.error_empty));
            return false;
        } else if (pass.length() < 6) {
            mEditTextPassword.requestFocus();
            mEditTextPassword.setError(getString(R.string.error_pass_length));
            return false;
        } else if (TextUtils.isEmpty(emergencyContact1)) {
            mEditTextEmergencyContact1.requestFocus();
            mEditTextEmergencyContact1.setError(getString(R.string.error_empty));
            return false;
        } else if (emergencyContact1.length() < 10) {
            mEditTextEmergencyContact1.requestFocus();
            mEditTextEmergencyContact1.setError(getString(R.string.error_phone_length));
            return false;
        } else if (TextUtils.isEmpty(emergencyContact2)) {
            mEditTextEmergencyContact2.requestFocus();
            mEditTextEmergencyContact2.setError(getString(R.string.error_empty));
            return false;
        } else if (emergencyContact2.length() < 10) {
            mEditTextEmergencyContact2.requestFocus();
            mEditTextEmergencyContact2.setError(getString(R.string.error_phone_length));
            return false;
        } else if (TextUtils.isEmpty(emergencyContact3)) {
            mEditTextEmergencyContact3.requestFocus();
            mEditTextEmergencyContact3.setError(getString(R.string.error_empty));
            return false;
        } else if (emergencyContact3.length() < 10) {
            mEditTextEmergencyContact3.requestFocus();
            mEditTextEmergencyContact3.setError(getString(R.string.error_phone_length));
            return false;
        } else if (TextUtils.isEmpty(age)) {
            mEditTextAge.requestFocus();
            mEditTextAge.setError(getString(R.string.error_empty));
            return false;
        } else if (TextUtils.isEmpty(height)) {
            mEditTextHeight.requestFocus();
            mEditTextHeight.setError(getString(R.string.error_empty));
            return false;
        }
        return true;
    }

    private JSONObject getSignUpRequestJson(String name, String phone, String pass,
            String emergencyContact1, String emergencyContact2, String emergencyContact3,
            String age, String height, boolean bp, String pocketType) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_NAME, name);
            jsonObject.put(KEY_PHONE, phone);
            jsonObject.put(KEY_PASSWORD, pass);
            jsonObject.put(KEY_EMERGENCY_CONTACT1, emergencyContact1);
            jsonObject.put(KEY_EMERGENCY_CONTACT2, emergencyContact2);
            jsonObject.put(KEY_EMERGENCY_CONTACT3, emergencyContact3);
            jsonObject.put(KEY_AGE, age);
            jsonObject.put(KEY_HEIGHT, height);
            jsonObject.put(KEY_BP, bp);
            jsonObject.put(KEY_POCKET_TYPE, pocketType);
            jsonObject.put(KEY_DEVICE_ID, MacAddress.getMacAddress());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void signUpUser(JSONObject jsonObject) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, SIGN_UP,
                jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                hideProgressDialog();
                handleSignUpResponse(response);
                launchClearStack(mContext, MainActivity.class);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                VolleyErrorHandler.handle(mContext, error);
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "sign_up");
    }

    private void handleSignUpResponse(JSONObject response) {
        try {
            int id = response.getInt(KEY_ID);
            String name = response.getString(KEY_NAME);
            String phone = response.getString(KEY_PHONE);
            String emergencyContact1 = response.getString(KEY_EMERGENCY_CONTACT1);
            String emergencyContact2 = response.getString(KEY_EMERGENCY_CONTACT2);
            String emergencyContact3 = response.getString(KEY_EMERGENCY_CONTACT3);
            int age = response.getInt(KEY_AGE);
            float height = (float) response.getDouble(KEY_HEIGHT);
            boolean bp = response.getBoolean(KEY_BP);
            String pocketType = response.getString(KEY_POCKET_TYPE);
            String token = response.getString(KEY_TOKEN);

            mPreferenceManager.setId(id);
            mPreferenceManager.setName(name);
            mPreferenceManager.setPhone(phone);
            mPreferenceManager.setEmergencyContact1(emergencyContact1);
            mPreferenceManager.setEmergencyContact2(emergencyContact2);
            mPreferenceManager.setEmergencyContact3(emergencyContact3);
            mPreferenceManager.setAge(age);
            mPreferenceManager.setHeight(height);
            mPreferenceManager.setBp(bp);
            mPreferenceManager.setPocketType(pocketType);
            mPreferenceManager.setToken(token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
