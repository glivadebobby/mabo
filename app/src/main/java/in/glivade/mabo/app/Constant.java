package in.glivade.mabo.app;

/**
 * Created by Bobby on 26-01-2017
 */

public class Constant {
    /**
     * Bundle Extras
     */
    public static final String EXTRA_POS_X = "pos_x";
    public static final String EXTRA_POS_Y = "pos_y";
    public static final String EXTRA_POS_Z = "pos_z";
    public static final String EXTRA_DIRECITON = "direction";
    /**
     * Constants
     */
    public static final String VALUE_SHIRT = "shirt";
    public static final String VALUE_PANT = "pant";
    public static final String VALUE_FRONT = "front";
    public static final String VALUE_BACK = "back";
}
