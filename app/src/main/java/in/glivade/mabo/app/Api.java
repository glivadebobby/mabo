package in.glivade.mabo.app;

/**
 * Created by Bobby on 26-01-2017
 */

public class Api {
    /**
     * Api Keys
     */
    public static final String KEY_NAME = "name";
    public static final String KEY_PHONE = "phone";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_EMERGENCY_CONTACT1 = "emergency_contact_1";
    public static final String KEY_EMERGENCY_CONTACT2 = "emergency_contact_2";
    public static final String KEY_EMERGENCY_CONTACT3 = "emergency_contact_3";
    public static final String KEY_AGE = "age";
    public static final String KEY_HEIGHT = "height";
    public static final String KEY_BP = "bp";
    public static final String KEY_POCKET_TYPE = "pocket_type";
    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_ID = "id";
    public static final String KEY_DEVICE_ID = "device_id";
    public static final String KEY_TOKEN = "token";
    public static final String KEY_POS_X = "pos_x";
    public static final String KEY_POS_Y = "pos_y";
    public static final String KEY_POS_Z = "pos_z";
    public static final String KEY_DIRECTION = "direction";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_LONGITUDE = "longitude";
    public static final String KEY_MESSAGE = "message";
    /**
     * Api URLS
     */
    private static final String URL = "http://mabo.glivade.in/api/";
    public static final String SIGN_UP = URL + "user/sign-up";
    public static final String SIGN_IN = URL + "user/sign-in";
    public static final String PROFILE = URL + "user/profile";
    public static final String FALL_DETECTION = URL + "fall-detection";
}
